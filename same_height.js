(function(){
  'use strict';

  var SameHeight = function(scope, items_selector, callback){
    if(!items_selector){ throw new Error('Items selector is not defined'); }

    this.items_selector = items_selector;
    this.$scope = $(scope);
    this.should_apply = typeof callback === 'undefined' ? true : callback();
    this.set_same_height();
  };

  SameHeight.prototype.set_same_height = function() {
    var max_height, height, $items, self = this;
      this.$scope.each(function(){
        max_height = 0;
        $items = $(self.items_selector, this).css({minHeight: ''});
          $items.each(function(){
            height = $(this).outerHeight();
            max_height = max_height > height ? max_height : height;
          }).css({minHeight: self.should_apply ? max_height : ''});
      }).promise().done(function(){
        self.should_apply && this.trigger('same_height');
      });
  };

  // Expose as jquery plugin
  var plugin = 'same_height';
  var Klass = SameHeight;

  $.fn[plugin] = function(selector, callback){
    $(this).each(function(){
        var $this = $(this);
        var instance = new Klass(this, selector, callback);
        $this.data(plugin, instance);
    });
    return this;
  };

  //Expose class
  $[Klass] = Klass;

})();


