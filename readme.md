# Same height plugin

## Overview
####Shorthand:
Sets same height for items within scope.


#### HTML before using same height.
```html
  <div class="row">
    <div class="item">
      <span class="content">Lorem ipsum dolor sit amet</span>
    </div>
    <div class="item">
      <span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus enim nulla. Suspendisse egestas a libero ac interdum. Suspendisse sed interdum ipsum.</span>
    </div>
    <div class="item">
      <span class="content">Lorem ipsum</span>
    </div>
    <div class="item">
      <span class="content"><h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4></span>
    </div>
  </div>
```
#### HTML after same height is applied.
```html
  <div class="row">
    <div class="item" style="min-height: 184px;">
      <span class="content">Lorem ipsum dolor sit amet</span>
    </div>
    <div class="item" style="min-height: 184px;">
      <span class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras maximus enim nulla. Suspendisse egestas a libero ac interdum. Suspendisse sed interdum ipsum.</span>
    </div>
    <div class="item" style="min-height: 184px;">
      <span class="content">Lorem ipsum</span>
    </div>
    <div class="item" style="min-height: 184px;">
      <span class="content"><h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4></span>
    </div>
  </div>
```

> ####How it works:
> Iterates through items within scope and takes the highest item height and sets it to other items min-height css property.


## Usage:

### Via Javascript
Enable manually with:
```javascript
  $('.row').same_height('.item');
```

### Options
| option       |type      | Description                                              |
| ----------   |--------- | ------------                                             |
| callback     |function  | callback function which needs to return a boolean truthy value. If ``true`` is returned plugin __will apply__ css ``min-height`` property. If ``false`` or nothing is returned same height __will remove__ css min-height property from selected elements. |

### Callback usage example
If for example you need to stop aligning items when window size is smaller than 400px you can use callback function like this: 

```javascript
  $(window).on('resize', function(){
    $('.row').same_height('.item', function(){
      // do something ...
      // return true to keep setting same height on items
    });
  });
```

### Callback usage example with <a href="https://effectiva@bitbucket.org/effectiva/jquery_detect_layout.git">detect layout</a> 
This example shows helpful combintation of using ``detect_layout`` and ``same_height`` plugins for responsive.
On layout change we detect layout size and if it's smaller than 768px same height is not applied, otherwise items height is adjusted according to the highest item within scope. 

```javascript
  $(window).on('responsive', function(e, layout){
    $('.row').same_height('.item', function(){
      if(layout.size < 768){
        return false;
      } 
    });
  });
```


### Events
| Event       | Description                                              |
| ------      | ------------                                             |
| same_height | event is fired immediately after same height is set      |


### Event usage example
```javascript
  $('.row').on('same_height', function(){
    console.log('same_height event triggered on ', $(this));
  });
```

### DEMO
For more information try provided demo.

In order to be able to try demo you need to do the following:

  + ``git clone`` this repository
  + run ``bower install``
  + run ``http-server``

### Install instructions
__To install latest version run:__

``bower install https://bitbucket.org/effectiva/jquery_same_height.git#master ``

__To install stable release version run:__

``bower install https://bitbucket.org/effectiva/jquery_same_height.git``
