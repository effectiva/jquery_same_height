'use strict';

$(function(){

  $('.row').on('same_height', function(){
    console.log('same_height event triggered on ', $(this));
  })
  .same_height('.item');

  $(window).on('resize', function(){
    $('.row').same_height('.item', function(){
      if(window.innerWidth > 767){
        return true;
      }
    });
  });

});
